Rebuild Access Permissions
----------------

This module provides a drush command to rebuild content access permissions. Specific content types can be specified
or excluded. The command runs as a batch process.

The basic command is:

drush rebuild-access-perms

You will usually want to pass the machine names of one or more content types as arguments, e.g.

drush rebuild-access-perms "basic page" article

If you do not pass any content types as arguments, permissions for all nodes will be rebuilt.  You may,
however, exclude content types from the rebuilding process, e.g.

drush rebuild-access-perms --exclude=article,"basic page"

By default nodes are processed in batches of 100. If you find that you are running out of memory,
or if you have a lot of memory and want to speed things up, you may specify a different batch size, e.g.

drush rebuild-access-perms --batch_size=10

Installation
-------------
Easy way: install and enable the module as normal.

Better way: to install without enabling yet another module, copy the folder to /drush or /sites/all/drush
in the current Drupal installation.

To use with all sites on the computer where Drush is installed, copy the module folder to a global location:
  Folders listed in the 'include' option (see drush topic docs-configuration).
  The system-wide Drush commands folder, e.g. /usr/share/drush/commands
  The ".drush" folder in the user's HOME folder.

In any event, before using the command, you should clear your drush cache (drush cc drush).
