<?php
/**
 * @file
 * Provide Drush command.
 */

/**
 * Implements hook_drush_help().
 */
function rebuild_access_perms_drush_help($section) {
  switch ($section) {
    case 'drush:rebuild-access-perms':
      return dt('Rebuild access permissions for specified content types.');
  }
  return '';
}

/**
 * Implements hook_drush_command().
 */
function rebuild_access_perms_drush_command() {
  $items = [];
  $items['rebuild-access-perms'] = [
    'description' => 'Rebuilds access permissions for specified content types.',
    'arguments' => [
      'types' => 'Machine names of content type(s) whose access should be rebuilt, separated by spaces.  If omitted, access to all nodes will be rebuilt.',
    ],
    'options' => [
      'exclude' => 'Comma delimited list of content types to exclude (e.g. article,"basic page"). Ignored if content types have been specified.',
      'batch_size' => 'Number of nodes to process at a time. Defaults to 100.',
    ],
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'core' => ['7.x'],
  ];
  return $items;
}

/**
 * Rebuilds access permissions for specified content types, or all.
 * @return mixed
 */
function drush_rebuild_access_perms() {
  $types = func_get_args();
  $exclude = drush_get_option('exclude');
  $excluded_types = $exclude ? array_filter(array_map('trim', explode(',', $exclude))) : [];

  $query = db_select('node', 'n')->fields('n', array('nid'));
  if (!empty($types)) {
    $query->condition('n.type', $types, 'IN');
    $types_msg = count($types) == 1 ? current($types) . ' nodes' : 'nodes of ' . rebuild_access_perms_oxford($types) . ' types';
  }
  elseif ($excluded_types) {
    $query->condition('n.type', $excluded_types, 'NOT IN');
    $types_msg = 'nodes excluding ' . rebuild_access_perms_oxford($excluded_types) . ' types';
  }
  else {
    $types_msg = 'nodes of all types';
  }
  $nids = $query->execute()->fetchCol();
  $total = count($nids);

  if (empty($total)) {
    return drush_set_error('Rebuild Access Perms', dt('No @types were found', ['@types' => $types_msg]));
  }

  if ($total > 1000) {
    drush_print(dt('Please note: running this command may take a while, and your site will be slow while it runs.'));
  }
  $confirm = drush_confirm(dt('Are you sure you want to rebuild permissions for @total @types?',
    [
      '@total' => $total,
      '@types' => $types_msg,
    ]));

  if (!$confirm) {
    return drush_user_abort();
  }

  $batch_size = drush_get_option('batch_size', 100);
  $progress = 0;
  $operations = [];
  foreach (array_chunk($nids, $batch_size) as $chunk) {
    $chunk_ct = count($chunk);
    $progress += $chunk_ct;
    $operations[] = [
      '_drush_rebuild_access_perms',
      [
        $chunk,
        dt('@percent% (Processing @progress of @total)', [
          '@percent' => round(100 * $progress / $total, 1),
          '@progress' => $progress,
          '@total' => $total,
        ]),
      ]
    ];
  }
  $batch = [
    'operations' => $operations,
    'title' => dt('Rebuild Access Permissions for @types', ['@types' => $types_msg]),
    'finished' => '_drush_rebuild_access_perms_finished',
    'progress_message' => dt('access to @current nodes of @total were rebuilt')
  ];
  batch_set($batch);
  $batch =& batch_get();
  $batch['progressive'] = FALSE;
  drush_backend_batch_process();
  return TRUE;
}

/**
 * Rebuilds the access permissions for a batch of nodes.
 *
 * @param array $chunk
 * @param array $details
 * @param array &$context
 */
function _drush_rebuild_access_perms($chunk, $details, &$context) {
  $context['message'] = dt('Rebuilding permissions for @count nodes @details', ['@count' => count($chunk), '@details' => $details]);
  // Make sure to initialize the results.
  if (!isset($context['results']['success'])) {
    $context['results']['success'] = 0;
  }
  $nodes = node_load_multiple($chunk);
  if ($nodes) {
    foreach ($nodes as $node) {
      node_access_acquire_grants($node);
      $context['results']['success']++;
    }
  }
}

/**
 * Tell user what happened.
 *
 * @see \_drush_batch_finished()
 *
 * @param bool $success
 * @param array $results
 * @param array $operations
 * @param string $elapsed
 */
function _drush_rebuild_access_perms_finished($success, $results, $operations, $elapsed) {
  if ($success) {
    // Let the user know we have finished.
    drush_log(dt('@succeeded operations succeeded. ', [
      '@succeeded' => empty($results['success']) ? 0 : $results['success'],
    ]), 'ok');
    drush_print(dt('You may (and may not!) want to unset the rebuild permissions flag with "@command"', ['@command' => 'drush vdel node_access_needs_rebuild']));
  }
}

/**
 * Utility function to format a list nicely.
 *
 * @param array $vars
 *
 * @return string
 */
function rebuild_access_perms_oxford(array $vars) {
  if (empty($vars)) {
    return '';
  }
  if (count($vars) == 1) {
    return current($vars);
  }
  $last = array_pop($vars);
  // Add a comma before the "and" if 3 or more total items.
  return implode(', ', $vars) . ((count($vars) > 1) ? ', ' : '') . ' and ' . $last;
}
